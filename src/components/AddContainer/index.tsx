import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView, Image } from "react-native";
import { styles } from "./styles";
import InputAdd from "../InputAdd";

type Props = {
    title: string;
    titulo_botao: string;
}

export default function AddContainer({title, titulo_botao}: Props) {

    return(
        <View style={styles.container}>
            <View style={styles.titulo}>
                <Image style={styles.imagem} source={require("../../assets/gym.png")}/>
                <Text style={styles.h1}>{title}</Text>
            </View>

            <View style={styles.info}>
                <InputAdd 
                    title="Nome"
                />

                <InputAdd 
                    title="Series"
                />

                <InputAdd 
                    title="Repetições"
                />
   
            </View>

            <TouchableOpacity style={styles.botao}>
                <Text style={styles.text}>{titulo_botao}</Text>
            </TouchableOpacity>

        </View>
    )
}