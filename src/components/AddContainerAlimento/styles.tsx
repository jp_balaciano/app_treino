import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
   container: {
    width: 320,
    height: 400,
    alignItems: 'center',
    gap: 20
   },
   
   titulo: {
    flexDirection: 'row',
    gap: 5,
    alignItems: 'center',
    marginTop: 30
   },

   imagem: {
    width: 42,
    height: 42
   },

   h1: {
    fontSize: 22,
    fontWeight: '400',
    color: '#FFFFFF'
   },

   info: {
    gap: 15
   },

   botao: {
      width: 109,
      height: 46,
      borderRadius: 13,
      backgroundColor: "#B200F1",
      marginTop: 30,
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 190
  },

  text: {
      fontSize: 16,
      fontWeight: "400",
      color: "#F1F1F1"
  },


})