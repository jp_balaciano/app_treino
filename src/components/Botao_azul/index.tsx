import { Text, View } from "react-native";
import { styles } from './styles';

type Props = {
    title: string;
}

export default function Botao_azul({title}: Props) {
    return(
        <View style={styles.botao}>
           <Text style={styles.texto}>{title}</Text>
        </View>
    )
}