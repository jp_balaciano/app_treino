import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    botao: {
        width: 369,
        height: 64,
        borderRadius: 20,
        backgroundColor: "#B600F6",
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center'
    },

    texto: {
        fontSize: 22,
        fontWeight: '600',
        color: '#FFFFFF'
    }
})