import { Text, View, Image, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { styles } from "./styles";




export default function Card_Dieta() {

   const navigation = useNavigation();
   function handleEditarAlimento(){
    navigation.navigate("EditarAlimento");
   }

    return(
        <View style={styles.card}>
            <Image source={require("../../assets/alimento.png")}/>
            <View style={styles.textos}>
                <Text style={styles.h1}>Nome do alimento</Text>
                <Text style={styles.h2}>Quantidade: 130 gramas</Text>
                <Text style={styles.h3}>Detalhes</Text>
            </View>
            

            <View style={styles.botoes}>
                <TouchableOpacity style={styles.botao} onPress={handleEditarAlimento}>
                    <Text style={styles.texto2}>Editar</Text>
                    <Image source={require("../../assets/editar.png")}/>
                </TouchableOpacity>

                <TouchableOpacity style={styles.botao}>
                    <Text style={styles.texto2}>Remover</Text>
                    <Image source={require("../../assets/lixo.png")}/>
                </TouchableOpacity>
            </View>
        </View>

    )
}