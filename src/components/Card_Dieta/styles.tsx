import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    card: {
        width: 360,
        minHeight: 62,
        height: "auto",
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        gap: 10,
    },

    textos: {
        flexDirection: 'column',
        width: "40%",
        height: "100%"
    },

    h1: {
        fontSize: 16,
        fontWeight: '400'
    },

    h2: {
        fontSize: 10,
        fontWeight: '300'
    },

    h3: {
        fontSize: 8,
        fontWeight: '200'
    },

    botoes: {
        flexDirection: 'row',
        gap: 5,
        alignItems: 'center'
    },

    botao: {
        flexDirection: 'row',
        gap: 5
    },

    texto2: {
        fontSize: 13,
        fontWeight: '200',
        color: 'black'
    },

})