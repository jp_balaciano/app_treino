import { Text, View, Image, TouchableOpacity } from "react-native";
import { styles } from './styles';
import { useNavigation } from "@react-navigation/native";




export default function Card_exercício() {

   const navigation = useNavigation();
   function handleEditarEx(){
    navigation.navigate("EditarEx");
   }

    return(
        <View style={styles.card}>
            <Image source={require("../../assets/exercicio.png")}/>
            <View style={styles.textos}>
                <Text style={styles.h1}>Nome do alimento</Text>
                <Text style={styles.h2}>Séries:</Text>
                <Text style={styles.h3}>Repetições:</Text>
            </View>


            <View style={styles.botoes}>
                <TouchableOpacity style={styles.botao} onPress={handleEditarEx}>
                    <Text style={styles.texto2}>Editar</Text>
                    <Image source={require("../../assets/editar.png")}/>
                </TouchableOpacity>

                <TouchableOpacity style={styles.botao}>
                    <Text style={styles.texto2}>Remover</Text>
                    <Image source={require("../../assets/lixo.png")}/>
                </TouchableOpacity>
            </View>
        </View>

    )
}