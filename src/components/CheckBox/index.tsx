import { Text, View, TextInput } from "react-native";
import { styles } from "./styles";
import CheckBoxIcon from "react-native-elements/dist/checkbox/CheckBoxIcon";
import { useState } from "react";

type Props = {
    title: string;
}

export default function Checkbox({title}: Props) {
    const [isChecked, setIsChecked] = useState(false);

    const handleCheckboxToggle = () => {
        setIsChecked(!isChecked);
    };

    return(
        <View style={styles.container}>
           <Text style={styles.text}>{title}</Text>
           <CheckBoxIcon  
                checked={isChecked}
                onIconPress={handleCheckboxToggle}
                checkedColor="blue"
           />
        </View>
    )
}