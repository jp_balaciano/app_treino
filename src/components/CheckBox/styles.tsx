import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 255,
        height: 64,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: "#FFFFFF",
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        gap: 35
    },

    text: {
        fontSize: 22,
        color: '#FFFFFF',
        fontWeight: '400',
    },

})