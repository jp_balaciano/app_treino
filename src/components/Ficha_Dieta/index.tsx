import { Text, View, Image, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import { useNavigation } from "@react-navigation/native";
import Card_Dieta from "../Card_Dieta";



export default function Ficha_Dieta() {
    const navigation = useNavigation();

    function handleAddAlimento() {
        navigation.navigate("AdicionarAlimento");
    }

    return(
        <View style={styles.container}>
            <View style={styles.titulo}>
                <Text style={styles.h1}>Refeição A</Text>
                <Text style={styles.h1}>Horário: 8h</Text>
            </View>


            <View style={styles.cards}>
                <Card_Dieta />
                <Card_Dieta />
            </View>


            <View style={styles.funcionalidades}>
                <Text style={styles.detail}>Detalhes:  </Text>

                <View style={styles.botoes}>
                    <TouchableOpacity onPress={handleAddAlimento} style={styles.parte}>
                        <Image source={require("../../assets/add.png")}/>
                        <Text>Adicionar Alimento</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.parte}>
                        <Image source={require("../../assets/remove.png")}/>
                        <Text>Remover Refeição</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View>
    )
}