import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 380,
        height: "auto",
        backgroundColor: "rgba(0, 0, 23, 1)",
        alignItems:'center',
        borderRadius: 10,
    },
    
    titulo: {
        flexDirection: 'row',
        padding: 10,
        gap: 140
    },

    h1: {
        fontSize: 21,
        fontWeight: '500',
        color: 'rgba(213, 244, 246, 1)'
    },

    cards:{
        gap: 15,
        marginBottom: 20,
    },

    funcionalidades: {
        width: 380,
        minHeight: 51,
        height: "auto",
        backgroundColor: "#FFFFFF",
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        flexDirection: 'column',
        gap: 10
    },

    detail: {
        fontSize: 13,
        fontWeight: '700',
        marginLeft: 20
    },

    botoes: {
        flexDirection: 'row',
        justifyContent: 'center',
        gap: 40,
        marginBottom: 5
    },

    parte: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 2
    }

})