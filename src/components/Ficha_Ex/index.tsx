import { Text, View, Image, TouchableOpacity } from "react-native";
import { styles } from './styles';
import Card_exercício from "../Card_Exercicio";
import { useNavigation } from "@react-navigation/native";



export default function Ficha() {
    const navigation = useNavigation();

    function handleAddEx() {
        navigation.navigate("AdicionarEx");
    }

    return(
        <View style={styles.container}>
            <View style={styles.titulo}>
                <Text style={styles.h1}>Treino A: Peito e tríceps</Text>
                <Text style={styles.h1}>Tempo: 1h30min</Text>
            </View>


            <View style={styles.cards}>
                <Card_exercício />
                <Card_exercício />
            </View>


            <View style={styles.funcionalidades}>
                <TouchableOpacity onPress={handleAddEx} style={styles.parte}>
                    <Image source={require("../../assets/add.png")}/>
                    <Text>Adicionar Exercício</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.parte}>
                    <Image source={require("../../assets/remove.png")}/>
                    <Text>Remover Treino</Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}