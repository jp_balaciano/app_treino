import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 360,
        height: "auto",
        backgroundColor: "rgba(0, 0, 23, 1)",
        alignItems:'center',
        borderRadius: 10,
    },
    
    titulo: {
        flexDirection: 'row',
        gap: 30,
        padding: 10
    },

    h1: {
        fontSize: 16,
        fontWeight: '500',
        color: 'rgba(213, 244, 246, 1)'
    },

    cards:{
        gap: 15,
        marginBottom: 20,
    },

    funcionalidades: {
        width: 360,
        height: 51,
        backgroundColor: "#FFFFFF",
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        gap: 40
    },

    parte: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 2
    }

})