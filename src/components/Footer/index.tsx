import { Image, View, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import { useNavigation } from "@react-navigation/native";


export default function Footer() {

    const navigation = useNavigation();

    function handleDieta(){
        navigation.navigate("Dieta");
    }

    function handleTreino(){
        navigation.navigate("Treino");
    }

    function handlePerfil(){
        navigation.navigate("Perfil");
    }


    return(
        <View style={styles.container}>
            <TouchableOpacity style={styles.content} onPress={handleDieta}>
                <Image source={require("../../assets/dieta.png")}/>
                <Text style={styles.titulo}>Dieta</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.content} onPress={handleTreino}>
                <Image source={require("../../assets/treino.png")}/>
                <Text style={styles.titulo}>Treino</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.content} onPress={handlePerfil}>
                <Image source={require("../../assets/perfil.png")}/>
                <Text style={styles.titulo}>Perfil</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.content}>
                <Image source={require("../../assets/projeto.png")}/>
                <Text style={styles.titulo}>Projeto</Text>
            </TouchableOpacity>
        </View>
    )
}