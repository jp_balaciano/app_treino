import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#01001e',
        width: '100%',
        height: 110,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        gap: 35
    },

    content: {
        flexDirection: "column",
        gap: 4
    },

    titulo: {
        fontSize: 13,
        fontWeight: '400',
        color: "white"
    }

    
})