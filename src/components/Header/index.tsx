import { Image, View, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import { useNavigation } from "@react-navigation/native";

type props = {
    title: string;
}

export default function Header({title}: props) {

    const navigation =useNavigation();

    function handleGoBack(){
        navigation.goBack();
    }

    return(
        <View style={styles.container}>
            <TouchableOpacity onPress={handleGoBack}><Image style={styles.imagem} source={require("../../assets/seta.png")}/></TouchableOpacity>
            <Text style={styles.titulo}>{title}</Text>
        </View>
    )
}