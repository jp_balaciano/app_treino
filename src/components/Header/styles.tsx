import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#01001e',
        width: '100%',
        height: 90,
        flexDirection: 'row',
        alignItems: 'center',
        gap: 5
    },

    imagem: {
        marginTop: 40,
        marginLeft: 30
    },

    titulo: {
        fontSize: 16,
        fontWeight: '600',
        color: '#D5F4F6',
        marginTop: 40
    },
})