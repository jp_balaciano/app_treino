import { Text, View, TextInput } from "react-native";
import { styles } from "./styles";

type Props = {
    title: string;
}

export default function InputAdd({title}: Props) {
    return(
        <View style={styles.dados}>
           <Text style={styles.texto}>{title}</Text>
           <TextInput style={styles.caixa} />
        </View>
    )
}