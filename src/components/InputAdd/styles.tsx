import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    dados: {
        flexDirection: 'column',
        gap: 3
    },
    
    texto: {
        fontSize: 14,
        fontWeight: '400',
        color: '#F1F1F1'
    },

    caixa: {
        width: 302,
        height: 27,
        borderRadius: 5,
        backgroundColor: 'white',
        fontSize: 16,
        fontWeight: '400',
        color: 'black',
        padding: 5
    }
})