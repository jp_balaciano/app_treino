import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 205,
        height: 205,
        borderRadius: 100,
        backgroundColor: '#F1F1F1',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 50
    }
})