import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "../screens/Login";
import Cadastro from "../screens/Cadastro";
import BemVindo from "../screens/BemVindo";
import Treino from "../screens/Treino";
import AdicionarEx from "../screens/AdicionarExercicio";
import EditarEx from "../screens/EditarExercicio";
import Dieta from "../screens/Dieta";
import AdicionarAlimento from "../screens/AdicionarAlimento";
import EditarAlimento from "../screens/EditarAlimento";
import Perfil from "../screens/Perfil";


const {Navigator, Screen} = createNativeStackNavigator();

export function AppRoutes() {
    return (
        <Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
            <Screen 
                name="Login"
                component={Login}
            />

            <Screen
                name="Cadastro"
                component={Cadastro}
            />

            <Screen 
                name="BemVindo"
                component={BemVindo}
            />

            <Screen 
                name="Treino"
                component={Treino}
            />

            <Screen 
                name="AdicionarEx"
                component={AdicionarEx}
            />

            <Screen 
                name="EditarEx"
                component={EditarEx}
            />

            <Screen 
                name="Dieta"
                component={Dieta}
            />

            <Screen 
                name="AdicionarAlimento"
                component={AdicionarAlimento}
            />

            <Screen 
                name="EditarAlimento"
                component={EditarAlimento}
            />

            <Screen 
                name="Perfil"
                component={Perfil}
            />
        </Navigator>

    )
}