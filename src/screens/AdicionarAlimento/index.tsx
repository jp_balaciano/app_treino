import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from "react-native";
import { styles } from "./styles";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import AddContainerAlimento from "../../components/AddContainerAlimento";




export default function AdicionarAlimento() {

    return(
        <>
        <Header 
            title="Adicionar Alimento"
        />
        <SafeAreaView style={styles.container}>
            <View  style={styles.content}>
                <AddContainerAlimento 
                    title="Adicionar Alimento"
                    titulo_botao="Adicionar"
                />
            </View>
        </SafeAreaView>
        <Footer />
        </>
    )
}