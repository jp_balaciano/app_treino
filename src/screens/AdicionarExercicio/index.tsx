import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from "react-native";
import { styles } from "./styles";
import Header from "../../components/Header";
import AddContainer from "../../components/AddContainer";
import Footer from "../../components/Footer";




export default function AdicionarEx() {

    return(
        <>
        <Header 
            title="Adicionar Exercício"
        />
        <SafeAreaView style={styles.container}>
            <View style={styles.content}>
                <AddContainer 
                    title="Adicionar Exercício"
                    titulo_botao="Adicionar"
                />
            </View>
        </SafeAreaView>
        <Footer/>

        </>
    )
}