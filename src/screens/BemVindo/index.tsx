import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from "react-native";
import { styles } from "./styles";
import Botao_rosa from "../../components/Botao_rosa";
import Botao_azul from "../../components/Botao_azul";



export default function BemVindo(props) {

    function handleTreino(){
        props.navigation.navigate('Treino');
    }
    function handleDieta(){
        props.navigation.navigate('Dieta');
    }

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.textos}>
                <View style={styles.titulos}>
                    <Text style={styles.h1}>Bem Vindo(a)</Text>
                    <Text style={styles.h2}>User Name</Text>
                </View>
                <Text style={styles.h3}>O que você deseja escrever?</Text>
            </View>
            
            <View style={styles.botoes}>
                <TouchableOpacity onPress={handleDieta}>
                    <Botao_rosa 
                        title="Dieta"
                    />
                </TouchableOpacity>
            
                <TouchableOpacity onPress={handleTreino}>
                    <Botao_azul 
                        title="Treino"
                    />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}