import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        alignItems: 'center',
        flexDirection: 'column',
        gap: 120,
        marginBottom: 80,
    },

    textos: {
        alignItems: 'center',
        marginTop: 80,
        gap: 60
    },

    titulos: {
        alignItems: 'center',
        gap: 10
    },

    h1: {
        fontSize: 30,
        fontWeight: '900',
        color: '#FFFFFF'
    },

    h2: {
        fontSize: 26,
        fontWeight: '600',
        color: '#FFFFFF'
    },

    h3: {
        fontSize: 18,
        fontWeight: '400',
        color: '#FFFFFF'
    },

    botoes: {
        gap: 30
    }

   

})