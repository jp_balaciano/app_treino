import { Text, View, TextInput, TouchableOpacity, ScrollView, Image } from "react-native";
import { styles } from './styles';
import Logo from "../../components/Logo";
import Botao_azul from "../../components/Botao_azul";
import Botao_rosa from "../../components/Botao_rosa";
import { useNavigation } from "@react-navigation/native";
import { useState } from "react";
import { CheckBox } from "react-native-elements";
import CheckBoxIcon from "react-native-elements/dist/checkbox/CheckBoxIcon";
import Checkbox from "../../components/CheckBox";

export default function Cadastro() {
    const [input, setInput] = useState(String);
    const [hidePass, setHidePass] = useState(true);

    const navigation = useNavigation();
    function handleLogin(){
        navigation.navigate('Login');
    }
    function handleEnter() {
        navigation.navigate('BemVindo');
    }

    return(
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.logo}>
                <Logo />
            </View>

            <View style={styles.inputArea}>
                    <TextInput style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="gray"
                    />
            </View>
            <View style={styles.senha}>
                <View style={styles.inputArea}>
                    <TextInput style={styles.input}
                        placeholder="Senha"
                        placeholderTextColor="gray"
                        value={input}
                        onChangeText={ (texto) => setInput(texto) }
                        secureTextEntry={hidePass}
                    />
                    
                    <TouchableOpacity onPress={ () => setHidePass(!hidePass)}>
                        { hidePass ? 
                            <Image style={styles.imagem} source={require("../../assets/senha2.png")}/>
                            :
                            <Image style={styles.imagem} source={require("../../assets/senha.png")}/>
                        }
                    </TouchableOpacity>
                </View>
                <View style={styles.inputArea}>
                    <TextInput style={styles.input}
                        placeholder="Confirmar senha"
                        placeholderTextColor="gray"
                        value={input}
                        onChangeText={ (texto) => setInput(texto) }
                        secureTextEntry={hidePass}
                    />
                    
                    <TouchableOpacity onPress={ () => setHidePass(!hidePass)}>
                        { hidePass ? 
                            <Image style={styles.imagem} source={require("../../assets/senha2.png")}/>
                            :
                            <Image style={styles.imagem} source={require("../../assets/senha.png")}/>
                        }
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.definition}>
                <Checkbox 
                    title="Projeto King"
                />
                <Checkbox 
                    title="Projeto Queen"
                />
            </View>

            <View style={styles.dados}>
                <TextInput style={styles.info}
                        placeholder="Peso"
                        placeholderTextColor="gray"
                />

                <TextInput style={styles.info}
                        placeholder="Altura"
                        placeholderTextColor="gray"
                />
            </View>

            <TextInput style={styles.info}
                placeholder="Idade"
                placeholderTextColor="gray"
            />

            <View style={styles.botoes}>
                <TouchableOpacity onPress={handleEnter}>
                    <Botao_azul 
                        title="Cadastre-se"
                    />
                </TouchableOpacity>
                <View style={styles.barra}></View>
                <View style={styles.partebaixo}>
                    <Text style={styles.jatem}>Já tem uma conta?</Text>
                    <TouchableOpacity onPress={handleLogin}>
                        <Botao_rosa 
                            title="Faça Login"
                        />
                    </TouchableOpacity>

                    
                </View>
            </View>
            
        </View>
        </ScrollView>
        
    )
}