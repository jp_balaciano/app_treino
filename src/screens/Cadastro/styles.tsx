import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        alignItems: 'center',
        flexDirection: 'column',
        gap: 30,
        marginBottom: 80,
    },

    logo: {
        marginTop: 80
    },

    inputArea: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 369,
        height: 64,
        borderRadius: 20,
        borderColor: 'white',
        borderWidth: 2,
    },

    input: {
        fontSize: 22,
        fontWeight: '400',
        color: 'white',
        padding: 10,
        width: '85%',
        height: 64
    },

    senha: {
        gap: 10
    },

    imagem: {
        width: 30,
        height: 30
    },

    definition: {
        gap: 15
    },

    dados: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        gap: 90
    },

    info: {
        width: 135,
        height: 64,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'white',
        fontSize: 22,
        fontWeight: '400',
        color: '#FFFFFF',
        padding: 10
    },

    botoes: {
        gap: 20
    },

    barra: {
        width: 281,
        height: 1,
        backgroundColor: "#D5F4F6",
        alignSelf: 'center',
    },

    partebaixo: {
        gap: 20
    },

    jatem: {
        fontSize: 16,
        fontWeight: '500',
        color: '#00F0FF',
        alignSelf: 'center'
    },

})