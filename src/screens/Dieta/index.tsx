import { Text, View, SafeAreaView, TextInput, TouchableOpacity, Image, ScrollView } from "react-native";
import { styles } from "./styles";

import Header from "../../components/Header";
import Footer from "../../components/Footer";


import Ficha_Dieta from "../../components/Ficha_Dieta";
import InputAdd from "../../components/InputAdd";

export default function Dieta() {

    return(
        <>
        <Header 
            title="Dieta: Nome da dieta"
        />
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <View style={styles.refeicoes}>
                    <Ficha_Dieta />
                    <Ficha_Dieta />
                </View>

                <View style={styles.add}>
                    <View style={styles.titulo}>
                        <Image source={require("../../assets/dieta.png")}/>
                        <Text style={styles.h1}>Adicionar Alimento</Text>
                    </View>
                    <View style={styles.boxes}>
                        <InputAdd 
                            title="Nome"
                        />
                        <InputAdd 
                            title="Duração"
                        />
                    </View>
                    <TouchableOpacity style={styles.botao_add}>
                        <Text style={styles.botao_add_txt}>Adicionar</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </SafeAreaView>
        <Footer />
        </>
    )
}