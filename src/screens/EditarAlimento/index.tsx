import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from "react-native";
import { styles } from "./styles";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import AddContainerAlimento from "../../components/AddContainerAlimento";



export default function EditarAlimento() {
    return(
        <>
        <Header 
            title="Editar Alimento"
        />
        <SafeAreaView style={styles.container}>
            <View style={styles.content}>
                <AddContainerAlimento 
                    title="Editar Alimento"
                    titulo_botao="Editar"
                />

            </View>
        </SafeAreaView>
        <Footer />
        </>
    )
}