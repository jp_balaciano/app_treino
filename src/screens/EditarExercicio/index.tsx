import { Text, View, SafeAreaView, TextInput, TouchableOpacity, ScrollView } from "react-native";
import { styles } from "./styles";
import Header from "../../components/Header";
import AddContainer from "../../components/AddContainer";
import Footer from "../../components/Footer";



export default function EditarEx() {
    return(
        <>
        <Header 
            title="Editar Exercício"
        />
        <SafeAreaView style={styles.container}>
            <View style={styles.content}>
                <AddContainer 
                    title="Editar Exercício"
                    titulo_botao="Editar"
                />

            </View>
        </SafeAreaView>
        <Footer />
        </>
    )
}