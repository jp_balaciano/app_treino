import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: "rgba(72, 5, 83, 0.9)",
        flex: 1,
    },

    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})