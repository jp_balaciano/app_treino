import { Text, View, SafeAreaView, TextInput, TouchableOpacity, Image } from "react-native";
import { styles } from './styles';
import Logo from "../../components/Logo";
import Botao_rosa from "../../components/Botao_rosa";
import Botao_azul from "../../components/Botao_azul";

import { useNavigation } from "@react-navigation/native";

import { useState } from "react";

export default function Login(props) {
    const [input, setInput] = useState(String);
    const [hidePass, setHidePass] = useState(true);

  

    const navigation = useNavigation();
    function handleEnter() {
        props.navigation.navigate('BemVindo');
    }
    function handleCadastro() {
        props.navigation.navigate('Cadastro');
    }

    return(
        <SafeAreaView style={styles.container}>
            <Logo />

            <View style={styles.content}>
                <View style={styles.inputArea}>
                    <TextInput style={styles.input}
                        placeholder="Email"
                        placeholderTextColor="gray"
                    />
                </View>

                <View style={styles.inputArea}>
                    <TextInput style={styles.input}
                        placeholder="Senha"
                        placeholderTextColor="gray"
                        value={input}
                        onChangeText={ (texto) => setInput(texto) }
                        secureTextEntry={hidePass}
                    />
                    
                    <TouchableOpacity onPress={ () => setHidePass(!hidePass)}>
                        { hidePass ? 
                            <Image style={styles.imagem} source={require("../../assets/senha2.png")}/>
                            :
                            <Image style={styles.imagem} source={require("../../assets/senha.png")}/>

                        }
                    </TouchableOpacity>
                </View>

            </View>
    
            <View style={styles.botoes}>
                <View style={styles.partecima}>
                    <TouchableOpacity onPress={handleEnter}>
                        <Botao_rosa 
                            title="Entrar"
                        />
                    </TouchableOpacity>

                    <TouchableOpacity><Text style={styles.esqueceu}>Esqueceu sua senha?</Text></TouchableOpacity>
                </View>

                <View style={styles.barra}></View>
                
                <TouchableOpacity onPress={handleCadastro}>
                    <Botao_azul 
                        title="Cadastre-se"
                    />
                </TouchableOpacity>
            </View>
            
        </SafeAreaView>
    )
}