import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 50
    },

    content: {
        flexDirection: 'column',
        gap: 20
    },

    inputArea: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 369,
        height: 64,
        borderRadius: 20,
        borderColor: 'white',
        borderWidth: 2,
    },

    input: {
        fontSize: 22,
        fontWeight: '400',
        color: 'white',
        padding: 10,
        width: '85%',
        height: 64
   },

   
   imagem: {
    width: 30,
    height: 30
   
   },


    botoes: {
        flexDirection: 'column',
        gap: 30
        
    },

    partecima: {
        flexDirection: 'column',
        gap: 20
    },

    esqueceu: {
        fontSize: 10,
        fontWeight: '300',
        color: '#00F0FF',
        marginLeft: 10
    },

    barra: {
        width: 281,
        height: 1,
        backgroundColor: "#D5F4F6",
        alignSelf: 'center',
    }
})