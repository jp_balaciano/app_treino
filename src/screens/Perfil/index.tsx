import { Text, View, SafeAreaView, TextInput, TouchableOpacity, Image } from "react-native";
import { styles } from './styles';
import Header from "../../components/Header";
import Footer from "../../components/Footer";



export default function Perfil() {
    return(
        <>
        <Header 
            title="Perfil"
        />


        <SafeAreaView style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.h1}>Informações</Text>

                <View style={styles.forms}>

                    <View style={styles.foto}>
                        <Text style={styles.imagem}>Foto</Text>
                        <Text style={styles.h2}>Nome</Text>
                    </View>

                    <View style={styles.info}>
                        <View style={styles.barra}>
                            <Text style={styles.h3}>Idade</Text>
                            <Text style={styles.h4}>22 anos</Text>
                        </View>

                        <View style={styles.barra}>
                            <Text style={styles.h3}>Altura</Text>
                            <Text style={styles.h4}>187 cm</Text>
                        </View>

                        <View style={styles.barra}>
                            <Text style={styles.h3}>Peso</Text>
                            <Text style={styles.h4}>85 kg</Text>
                        </View>

                        <View style={styles.barra}>
                            <Text style={styles.h3}>Projeto</Text>
                            <Text style={styles.h4}>King</Text>
                        </View>

                        <View style={styles.barra}>
                            <Text style={styles.h3}>Objetivo</Text>
                            <Text style={styles.h4}>Perder Peso</Text>
                        </View>
                    </View>
                </View>
            </View>
        </SafeAreaView>
        <Footer />
        </>

    )
}