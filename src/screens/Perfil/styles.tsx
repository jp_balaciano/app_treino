import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(72, 5, 83, 0.9)",
        alignItems: "center",
        justifyContent: 'center'
    },

    content: {
        width: 360,
        height: 540,
        backgroundColor: "#D9D9D9",
        gap: 30
    },

    h1: {
        fontSize: 16,
        fontWeight: '600',
        marginLeft: 10,
        marginTop: 10
    },

    forms: {
        alignItems: 'center',
    },

    foto: {
        alignItems: 'center',
        marginBottom: 50,
        gap: 10
    },

    imagem: {
        backgroundColor: "#D5F4F6",
        width: 100,
        height: 100,
    },

    h2: {
        fontSize: 16,
        fontWeight: '600',
    },

    info: {
        flexDirection: 'column',
        gap: 15
    },

    barra: {
        width: 338,
        height: 33,
        backgroundColor: "#D5F4F6",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 5
    },

    h3: {
        fontSize: 16,
        fontWeight: '600'
    },

    h4: {
        fontSize: 16,
        fontWeight: '400'
    }
})