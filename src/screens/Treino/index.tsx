import { Text, View, SafeAreaView, Image, TouchableOpacity, ScrollView } from "react-native";
import { styles } from './styles';
import Ficha from "../../components/Ficha_Ex";
import InputAdd from "../../components/InputAdd";
import Header from "../../components/Header";
import Footer from "../../components/Footer";


export default function Treino() {
    
    return(
        <>
        <Header 
                title="Ficha"
        />
        <SafeAreaView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <View style={styles.fichas}>
                    <Ficha />
                    <Ficha />
                </View>

                <View style={styles.add}>
                    <View style={styles.titulo}>
                        <Image source={require("../../assets/gym.png")}/>
                        <Text style={styles.h1}>Adicionar Treino</Text>
                    </View>
                    <View style={styles.boxes}>
                        <InputAdd 
                            title="Nome"
                        />
                        <InputAdd 
                            title="Duração"
                        />
                    </View>
                    <TouchableOpacity style={styles.botao_add}>
                        <Text style={styles.botao_add_txt}>Adicionar</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </SafeAreaView>
        <Footer />

        </>
    )
}