import { StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: "rgba(72, 5, 83, 0.9)",
        flex: 1,
    },

    content: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        marginTop: 40,
        gap: 50
    },


    fichas: {
        gap: 15,
    },

    add: {
        width: "100%",
        height: 277,
        backgroundColor: 'black',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        gap: 10,
        marginTop: 15,
    },

    titulo: {
        flexDirection: 'row',
        gap: 15,
        marginRight: 120
    },

    h1: {
        fontSize: 18,
        fontWeight: '600',
        color: "#F1F1F1"
    },

    boxes: {
        flexDirection: 'column',
        gap: 20
    },

    botao_add: {
        width: 90,
        height: 38,
        borderRadius: 5,
        backgroundColor: '#B600F6',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 210,
        marginTop: 30
    },

    botao_add_txt: {
        color: '#F1F1F1',
        fontSize: 15,
        fontWeight: '400'
    }




   
})